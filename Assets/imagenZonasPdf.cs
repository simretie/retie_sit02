﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSAvancedUI;

public class imagenZonasPdf : MonoBehaviour {

    [SerializeField]
    private GameObject HojaDOndeSeEncuentraLaGrid;

    [SerializeField]
    private bool practicauno;
	public bool PracticaDos = false;

    [SerializeField]
    private GameObject[] Zonas;

    [SerializeField]
    private GameObject[] terurometoDeZonas;

    [SerializeField]
    private RectTransform GridDelpdfDondeVanlasIamgenesDeLasZnas;
    


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InsertarZonasPdf()
    {
        HojaDOndeSeEncuentraLaGrid.SetActive(true);
        activarYDesactivarTelurometro(false);
        if (Zonas.Length>0)
        {
            var aux = GridDelpdfDondeVanlasIamgenesDeLasZnas.transform.childCount;
            for (int i = 0; i < aux; i++)
            {
                DestroyImmediate(GridDelpdfDondeVanlasIamgenesDeLasZnas.GetChild(0).gameObject);
            }
        }
               
        for (int i = 0; i < Zonas.Length; i++)
        {
     
           var Auxzona = Instantiate(Zonas[i], GridDelpdfDondeVanlasIamgenesDeLasZnas);

            if (practicauno)
            {
                if (i == 1|| i==2)
                {
                    Auxzona.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(0, -100f, 0);
                }

            }

			if(PracticaDos){

				SelectorRetie3[] selectoresZona = Auxzona.GetComponentsInChildren<SelectorRetie3>();

				for(var l = 0; l<selectoresZona.Length; l++){

					selectoresZona[l].CerrarPrincipal = false;

				}
				Auxzona.GetComponent<panelZonas> ().enabled = false;
				Auxzona.transform.localScale = new Vector3 (0.07f,0.07f,0.07f);
			}

            Auxzona.GetComponent<RectTransform>().localScale = new Vector3(0.07f, 0.07f, 1);
			Auxzona.GetComponent<RectTransform> ().localPosition = new Vector3 (Auxzona.GetComponent<RectTransform>().localPosition.x,Auxzona.GetComponent<RectTransform>().localPosition.y,0f);
            Auxzona.SetActive(true);
            Auxzona.GetComponent<CanvasGroup>().alpha = 1;
            //activarYDesactivarTelurometro(true);

        }


    }

    private void activarYDesactivarTelurometro(bool activo)
    {
        Debug.Log("telurometros "+activo);
        for (int i = 0; i < terurometoDeZonas.Length; i++)
        {
            terurometoDeZonas[i].SetActive(activo);
        }
    }


   /* private IEnumerator CouCapturarImagePDF()
    {
        capturaHojasPdf = new Texture2D[transform.childCount];
        cameraCapturaPDF.gameObject.SetActive(true);
        transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(1);

        for (int i = 0; i < transform.childCount; i++)
        {
            var tmpGameObjectHojaPdfActual = transform.GetChild(i).gameObject;
            tmpGameObjectHojaPdfActual.SetActive(true);
            //LayoutRebuilder.ForceRebuildLayoutImmediate(tmpGameObjectHojaPdfActual.GetComponent<RectTransform>());
            yield return null;
            cameraCapturaPDF.Render();
            RenderTexture.active = renderTexturePDF;
            var tmpTexture2D = new Texture2D(824, 1080, TextureFormat.ARGB32, false, true);
            tmpTexture2D.ReadPixels(new Rect(548, 0, 824, 1080), 0, 0);
            tmpTexture2D.Apply();
            capturaHojasPdf[i] = tmpTexture2D;
            RenderTexture.active = null;
            tmpGameObjectHojaPdfActual.SetActive(false);

        }
        cameraCapturaPDF.gameObject.SetActive(false);
        Debug.Log("Este es el codigo_______________________ " + codigoActividad);
        refEnvioPdf.mtdDescargaryEnviarReporte(codigoActividad, refControladorDatosSesion._cantidadIntentos.ToString(), tiempoSituacion, refEvaluacion.GetCalificacionTotalRango(), nombreSituacion, capturaHojasPdf);
    }*/


}
