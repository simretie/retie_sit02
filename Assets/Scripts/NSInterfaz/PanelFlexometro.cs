﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSAvancedUI;
using TMPro;

namespace NSInterfaz
{
    public class PanelFlexometro : AbstractPanelUIAnimation
    {
        #region members
        [SerializeField]
        private TextMeshProUGUI pantallaFlexometro;
            
        #endregion

        #region delegate

        public delegate void MostrarValor(float valorMetros);

        public MostrarValor ImprimirValor;

        #endregion


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void MostrarValorEnPantallaFlexometro(float valor)
        {
            pantallaFlexometro.text = valor.ToString("0.##")+"m";
        }


        public void ReiniciarPanleFlexomero()
        {
            pantallaFlexometro.text = "0m";
        }


        public void Mostrar2(bool activar=true)
        {
            if (gameObject.activeSelf)
            {
                ReiniciarPanleFlexomero();
            }
            else
            {
                Mostrar(activar);
            }

        }


    }

}
