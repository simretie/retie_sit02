﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace NSInterfaz


{
    public class PanelBarraUsuario : MonoBehaviour
    {
        public TextMeshProUGUI NombreUser;
        public TextMeshProUGUI TimepoPractica;
        public TextMeshProUGUI TextIntentos;

        [SerializeField]
        private Animator animatorBarraInformacionUsuario;

        public void OnButtonDesplegarContraerMenu()
        {
            animatorBarraInformacionUsuario.SetBool("desplegar", !animatorBarraInformacionUsuario.GetBool("desplegar"));
		
		}IEnumerator Start(){

			yield return new WaitForSeconds(2f);
			animatorBarraInformacionUsuario.SetBool("desplegar", true);

		}





    } 
}
