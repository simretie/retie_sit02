﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSAvancedUI;
using UnityEngine.UI;
using TMPro;

public class PanelTransformado : AbstractPanelUIAnimation
{

    //private Image PlacaTransformador;

    public GameObject botones; 
    public TextMeshProUGUI Kvea;
    public TextMeshProUGUI MasaTotalTransformador;

    private int tamañoPosteCorrecto;

    private List<transformador> valoreParaELtrasnformadorEnKvea;

    public transformador ConfiguracionSeleccionada;

    private void Awake()
    {
        valoreParaELtrasnformadorEnKvea = new List<transformador>();
        llenarLista();
    }

    // Use this for initialization
    void Start() {

        InitValoresplacasTransformador();
        ConfiguracionSeleccionada = new transformador(1f,1,1);
    }

    // Update is called once per frame
    void Update() {

    }

    public void llenarLista()
    {
        valoreParaELtrasnformadorEnKvea.Add(new transformador(10f,150,10));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(30f,295, 10));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(45f,370, 12));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(75f, 500, 12));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(112.5f, 550,12));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(112.5f, 650, 12));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(150f, 600, 12));
        valoreParaELtrasnformadorEnKvea.Add(new transformador(150f, 750, 12));


    }

    public void InitValoresplacasTransformador()
    {
        Debug.Log("hola");
        var dado = Random.Range(7, 0);
        ConfiguracionSeleccionada = valoreParaELtrasnformadorEnKvea[dado];

        Debug.Log(ConfiguracionSeleccionada.KVA.ToString());
        Debug.Log(ConfiguracionSeleccionada.PESO.ToString());


        Kvea.text = ConfiguracionSeleccionada.KVA.ToString();
        MasaTotalTransformador.text = ConfiguracionSeleccionada.PESO.ToString();
        tamañoPosteCorrecto = ConfiguracionSeleccionada.MetrosDelPostecorrecto;

    }

    public void reiniciartransformador()
    {
        var dado = Random.Range(7, 0);
        ConfiguracionSeleccionada = valoreParaELtrasnformadorEnKvea[dado];
    }


    public void SalirDelaPlaca()
    {
        botones.SetActive(true);
        this.gameObject.SetActive(false);

    }


}
