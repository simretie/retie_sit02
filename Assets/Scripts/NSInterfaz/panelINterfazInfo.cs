﻿using NSAvancedUI;
using TMPro;
using UnityEngine;

namespace NSInterfaz
{
    public class panelINterfazInfo : AbstractPanelUIAnimation
    {

        #region members


        public GameObject panelDeInfromacionActivo;


        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake()
        {

        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        #endregion

        #region private methods

        #endregion

        #region public methods



        public void MtdActivarInfoSituacionDeLaSituacionActual()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarSituacion();
        }

        public void MtdActivarInfoEcuacionesDeLaSituacionActual()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarEcuaciones();
        }

        public void MtdActivarInfoProcedimientoDeLaSituacionActual()
        {
            panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarProcedimeinto();
        }

        #endregion

        #region courutines

        #endregion
    }

}