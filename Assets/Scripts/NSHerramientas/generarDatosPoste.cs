﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSBoxMessage;


public class generarDatosPoste : MonoBehaviour {


    public float longitudPoste;

    public float alturaVisible;

    public float ProfundidadEmpotramiento;
    //----------------------------------------

    [SerializeField]
    private float rangoMinimoHProfundidad;

    [SerializeField]
    private float rangoMaximoHprofundidad;

    ///---------------------------------------
    [SerializeField]
    private InputField profundidadEmpotramiento;
        
    [SerializeField]
    private bool EsAleatorio;

    [SerializeField]
    private float valor1Deposte;

    [SerializeField]
    private float valor2Deposte;

    [SerializeField]
    private GameObject placavalor1;
    [SerializeField]
    private float[] valorMinimo_Maximo1;


    [SerializeField]
    private GameObject placavalor2;
    [SerializeField]
    private float[] valorMinimo_Maximo2;

    [SerializeField]
    private ManagerZona7 MangerZona7; 


    private bool Esvalor1=false;
   
    private float ValorParacumplirdeProfundidadEmpotramiento;


    private void Awake()
    {
        if (EsAleatorio)
        {
            initPoste();
        }
        else
        {
            calcularHPorfundidad();
        }
    }
     
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void initPoste()
    {
        var moneda = Random.Range(10, 0);


        if (5 < moneda)
        {
            longitudPoste = valor1Deposte;
            placavalor1.SetActive(true);
            placavalor2.SetActive(false);
            rangoMinimoHProfundidad = valorMinimo_Maximo1[0];
            rangoMaximoHprofundidad = valorMinimo_Maximo1[1];
            ProfundidadEmpotramiento = valorMinimo_Maximo1[2];
            Esvalor1 = true;
        }
        else
        {
            longitudPoste = valor2Deposte;
            placavalor1.SetActive(false);
            placavalor2.SetActive(true);
            rangoMinimoHProfundidad = valorMinimo_Maximo2[0];
            rangoMaximoHprofundidad = valorMinimo_Maximo2[1];
            ProfundidadEmpotramiento = valorMinimo_Maximo2[2];
            Esvalor1 = false;
        }
    }

    private void calcularHPorfundidad()
    {
        ProfundidadEmpotramiento = Random.Range(rangoMaximoHprofundidad, rangoMinimoHProfundidad);

        alturaVisible = longitudPoste - ProfundidadEmpotramiento;
    }


    public void InsertarProfundidad()
    {
        ProfundidadEmpotramiento = float.Parse(profundidadEmpotramiento.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

        if (Esvalor1)
        {
            if (ProfundidadEmpotramiento >= valorMinimo_Maximo1[0] && valorMinimo_Maximo1[1] >= ProfundidadEmpotramiento)
            {
                alturaVisible = longitudPoste - ProfundidadEmpotramiento;
                MangerZona7.SecuenciaZona7(true);
            }
            else
            {
                ProfundidadEmpotramiento = 0f;
                BoxMessageManager._instance.MtdCreateBoxMessageInfo("La profundidad de empotramiento del poste esta por debajo de lo establecido en el reglamento RETIE o es demasiado profundo, revise nuevamente sus cálculos.", "ACEPTAR");
            }
        }
        else
        {
            if (ProfundidadEmpotramiento >= valorMinimo_Maximo2[0] && valorMinimo_Maximo2[1] >= ProfundidadEmpotramiento)
            {
                MangerZona7.SecuenciaZona7(true);
                alturaVisible = longitudPoste - ProfundidadEmpotramiento;
            }
            else
            {
                ProfundidadEmpotramiento = 0f;
                BoxMessageManager._instance.MtdCreateBoxMessageInfo("La profundidad de empotramiento del poste esta por debajo de lo establecido en el reglamento RETIE o es demasiado profundo, revise nuevamente sus cálculos.", "ACEPTAR");
            }
        }

  
    }


    public void ReiniciarValoresPoste()
    {
        if (EsAleatorio)
            initPoste();

        calcularHPorfundidad();
    }




}
