﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectorRetie3 : MonoBehaviour {

	// Use this for initialization
	public int ActualSeleccionado = 0;
 
	[Header("Ingreso Datos")]
	public bool HabilitarRegistroDatos;
	public Dropdown[] ingresoDatos;

	[Header("Alterar Escenario")]
	public bool Alterar = false;
	public GameObject[] objetos;

	[Header("Alterar Escenario Principal")]
	public bool EscenarioPrincipal = false;
	public GameObject[] EscenarioPObjectos;

	public bool CerrarPrincipal = false;
	public panelZonas zonaPrincipal;

	[Header("Zona Alterntiva")]
	public bool ZonaAlternativa = false;
	public GameObject[] ZonaAltObj;
	public bool ZonaAlternativa1 = false;
	public GameObject[] ZonaAltObj1;
	public bool ZonaAlternativa2 = false;
	public GameObject[] ZonaAltObj2;

	[Header("Retie 4 ingreso dato")]
	public bool HabilitarIngresoD = false;
	public GameObject InputDatos;
	public int indexActivar = 0;

	[Header("Retie 4 Zona Ocultar")]
	public bool ZonaOcultar = false;
	public GameObject ZonaOcultaGO;
	public GameObject ZonaOcultaGO1;
	public int indexZonaOcultar = 0;

	private bool HabilitarModificacion = true;
	public bool IgnorarStart = false;
	public bool FlexoHabilitado = false;
	public bool PotenciaTransformador = false;

	void Start () {

		if(!IgnorarStart)
			RandomStart (); 
		if (CerrarPrincipal)
			zonaPrincipal.Mostrar (false);
	}
	
	// Update is called once per frame
	public void EnableMod (bool state) {

		HabilitarModificacion = state;

	}public void OpcionElegir(int pos){

		if(HabilitarModificacion){
			ActualSeleccionado = pos;
			//GetComponent<panelZonas>().Mostrar(false); // para cerrar la ventana al seleccionar una opción
			AlterarEscenario();
			if(FlexoHabilitado)
				GameObject.FindObjectOfType<CalificarRetie4>().SetFlexoHabilitado(ActualSeleccionado);
			if(PotenciaTransformador)
				GameObject.FindObjectOfType<CalificarRetie4>().SetPotenciaTransformador(ActualSeleccionado);

		}

	}public void RandomStart(int estadoInicial = -1){

		int ranStart = 0;
		if(estadoInicial == -1)
			ranStart = Random.Range(0,transform.childCount);
		else
			ranStart = estadoInicial;

		for(var i = 0; i<transform.childCount; i++){

			if(i==ranStart){
			
				transform.GetChild(i).transform.Find("contenedor").transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(i).transform.Find("contenedor").transform.GetChild(1).gameObject.SetActive(false);

			}else{

				transform.GetChild(i).transform.Find("contenedor").transform.GetChild(0).gameObject.SetActive(false);
				transform.GetChild(i).transform.Find("contenedor").transform.GetChild(1).gameObject.SetActive(true);

			}

			ActualSeleccionado = ranStart;
			AlterarEscenario();

		}
		GetComponent<panelZonas>().Mostrar(false);

		if(ZonaOcultar && indexZonaOcultar == ActualSeleccionado){

			ZonaOcultaGO.SetActive(false);
			ZonaOcultaGO1.SetActive(false);

		}else{

			if(ZonaOcultar){
				ZonaOcultaGO.SetActive(true);
				ZonaOcultaGO1.SetActive(true);
			}
		}

		if(FlexoHabilitado)
			GameObject.FindObjectOfType<CalificarRetie4>().SetFlexoHabilitado(ActualSeleccionado);
		if(PotenciaTransformador)
			GameObject.FindObjectOfType<CalificarRetie4>().SetPotenciaTransformador(ActualSeleccionado);

		VerificarInput();

	}public void AlterarEscenario(){

		if(Alterar){

			for(var i = 0; i<objetos.Length; i++){

				if(i == ActualSeleccionado){
					objetos[i].SetActive(true);
					if(EscenarioPrincipal){
						EscenarioPObjectos[i].SetActive(true);
					}if(ZonaAlternativa){
						ZonaAltObj[i].SetActive(true);
					}if(ZonaAlternativa1){
						ZonaAltObj1[i].SetActive(true);
					}if(ZonaAlternativa2){
						ZonaAltObj2[i].SetActive(true);
					}
				}else{
					objetos[i].SetActive(false);
					if(EscenarioPrincipal){
						EscenarioPObjectos[i].SetActive(false);
					}if(ZonaAlternativa){
						ZonaAltObj[i].SetActive(false);
					}if(ZonaAlternativa1){
						ZonaAltObj1[i].SetActive(false);
					}if(ZonaAlternativa2){
						ZonaAltObj2[i].SetActive(false);
					}
				}
			}

		}

	}public void VerificarInput(){

		if(HabilitarIngresoD && ActualSeleccionado == indexActivar){

			InputDatos.GetComponent<PanelDatosMalla>().MostrarMalla(ActualSeleccionado);

		}

	}public void ReStart () {

 		if (CerrarPrincipal)
			zonaPrincipal.Mostrar (false);
	}

}
