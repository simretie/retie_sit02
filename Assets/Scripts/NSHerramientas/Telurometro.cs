﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSBoxMessage;
using NSInterfaz;
using NSInterfazAvanzada;
using NSTraduccionIdiomas;


public class Telurometro : CalculoResistencia {

	// Use this for initialization

	[Header("Valor Fijo")]
	[SerializeField] private bool evaluarValorFijo = false;
	[SerializeField] private float margenDeErrorValorFijo = 0.0001f;
	[Space]
	[SerializeField] private bool usarValorCalculado = true;
	[SerializeField] private float valorFijo = 0f;

	[Header("SPTs")]
	public panelZonas PanelACerrar;
	public int[] SPT_Permitidos;
	public GameObject[] RootOpcionesVisual;
	public GameObject[] BotonesSeleccionSPT;
	private int LastSelected;

	// solo para la zona 2
	public int zonaErrorSelected = -1;
	private int OpcionAElegir = 0;     // El SPT Actual
	private int actualSelectedZ = -1;

	[Header("Picas")]
	private int picaActual = 0;
	public GameObject[] Picas;
	public GameObject BotonPicas;
	public string[] textoPicasInput;
	public TMPro.TextMeshProUGUI enunciado;

	[Header("Input")]
	public TMPro.TMP_InputField InputDistancia;
	public int MaxValue;
	public GameObject BotonGrafico;
	public float DistanciaPica1 = 15.86f;
	public bool Zona0ErroFalse = false;
	public GameObject[] ReferenciasMedidas;

	[Header("Pinzas")]
	public GameObject TelurometroBotón;
	public GameObject PinzasRoot;
	public bool PinzaParaMalla = false;
	private bool TelurometroConfigurado = false;
	public GameObject[] Pinzas;
	private Vector3[] PinzasStartPos = new Vector3[3];
	public Sprite[] PinzasEstado;
	private int PinzaTelurometro = 0;
	private bool PuedePinzasTelu = false;
	private bool TelurometroActivado = false;
	public Transform[] ObjetosEnlace;
	public float DistanciaEnlace = 50;
	public float DistVerEnlace = 10;
	private bool enTelurometro = false;
	private bool PinzaEnMalla = false;
 
	[Header("Porcentajes %")]
 	public float RangoError;		// porcentaje de error al poner pica basado sobre la distancia de la primera pica
	public float[] DatosRecolectados;
	public bool NoAlterarPosMedidas = false;

	[Header("Botón Accionar")]
	public GameObject[] botonesTelu;


	[Header("Configuración zona 2")]
	public Transform[] Posiciones;
	public Transform PinzasReference;

	[Header("Zonas Activas Picas")]
	public Image[] Resplandores;

	// control telurometro desde la pica
	private bool EnTelurometroParaPica = false;

	public Telurometro[] telurosZona2;
	private bool iniciado = false;

	void Start () {

		textoPicasInput [0] = DiccionarioIdiomas._instance.Traducir ("MensajeDistanciaEnterramientoPica1", "Distancia de enterramiento <br> pica de corriente.");
		textoPicasInput [1] = DiccionarioIdiomas._instance.Traducir ("MensajeDistanciaEnterramientoPica2", "Distancia de enterramiento <br> pica de potencial.");

		if (!iniciado) {
				PinzasStartPos [0] = Pinzas [0].transform.localPosition;
				PinzasStartPos [1] = Pinzas [1].transform.localPosition;
				PinzasStartPos [2] = Pinzas [2].transform.localPosition;
				iniciado = true;
		}

		TelurometroBotón.GetComponent<Button> ().enabled = false;
		TelurometroBotón.GetComponent<Animator> ().SetBool ("activar", false);

		Init();

		DatosRecolectados = new float[9];

		for(var i = 0; i<BotonesSeleccionSPT.Length; i++){


			for(var l = 0; l<SPT_Permitidos.Length; l++){

				if (i == SPT_Permitidos [l]) {

					BotonesSeleccionSPT [SPT_Permitidos[l]].SetActive (true);

				}

			}


		}

		if (Zona0ErroFalse) {

			if (OpcionAElegir == 0){
				GameObject.FindObjectOfType<CalificarRetie2> ().EnZona0Error = true;
			}else {
				GameObject.FindObjectOfType<CalificarRetie2> ().EnZona0Error = false;
			}
		}
	}

	private void OnEnable()
	{
		// Si la data está verificada
		if (VerifyData())
			DatosCompletos(); // Comunico los datos a la gráfica
	}
	private bool yaPuedePinza = false;
	// Update is called once per frame
	void FixedUpdate () {

		if (Input.GetButton ("Fire1") && yaPuedePinza) {

			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit)) {
				if ((hit.collider.name == "PinzaCollider" || hit.collider.name == "PinzaColliderMalla") && PinzaTelurometro != -1) {
					Pinzas [PinzaTelurometro].transform.position = hit.point;
					Pinzas [PinzaTelurometro].transform.GetChild (0).GetComponent<Image> ().sprite = PinzasEstado [PinzaTelurometro];

					for (var i = 0; i < ObjetosEnlace.Length; i++) {


						if (PinzaParaMalla && i == 0) { // si pinzaparamalla es true, se ignora la primera pinza (verde)
						
						} else if (Vector3.Distance (Pinzas [PinzaTelurometro].transform.position, ObjetosEnlace [i].position) < DistanciaEnlace) {
					
							Pinzas [PinzaTelurometro].transform.position = ObjetosEnlace [i].position;
							Pinzas [PinzaTelurometro].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (0.65f, 0.65f, 0.65f);
							break;

						} else {
						
							Pinzas [PinzaTelurometro].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (1,1,1);


						}

					}
			
				}
				if (hit.collider.name == "PinzaColliderMalla" && PinzaTelurometro == 0 && PinzaParaMalla) {

					PinzaEnMalla = true;
					Pinzas [PinzaTelurometro].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (0.65f, 0.65f, 0.65f);

				} else if (hit.collider.name != "PinzaColliderMalla" && PinzaTelurometro == 0 && PinzaParaMalla) {

					PinzaEnMalla = false;
					Pinzas [PinzaTelurometro].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (1,1,1);

				}
			}
			VerificarCofigPinzas ();

		}if (TelurometroConfigurado) {
		
		
		
		} else { // cuando se desconecta alguna de las pinzas
		
			if (TelurometroActivado) {

				ActivarTelurometro (false);

			}
		
		}

	}





	public void ActivarTelurometro(bool state){
	
		TelurometroActivado = state;
		if (enTelurometro && TelurometroConfigurado) {

			botonesTelu[0].SetActive(!TelurometroActivado);
			botonesTelu[1].SetActive(TelurometroActivado);
			RecogerDatos();

		}if (!state) {
			
			botonesTelu [0].SetActive (true);
			botonesTelu [1].SetActive (false);
			TextoResistencia.text = "00.00";

		} else {
		
			VerificarCofigPinzas (true);
		
		}

	
	}public void OpcionElegida(int pos){

		OpcionAElegir = pos;


	}public void ValidarOpcion(bool state = true){

		if(!state){

			OpcionAElegir = LastSelected;

		}else{// borra los datos temporales porque se cambia el spt y la resistencia se modifica, datos nuevos deben ser recolectados

			DatosRecolectados = new float[9];
			RecogerDatos();
			BotonGrafico.GetComponent<Button>().interactable = false;
			LastSelected = OpcionAElegir;
			RestaurarMedidasPorPica();
		}


		for(var i = 0; i<RootOpcionesVisual.Length; i++){

			if(OpcionAElegir == i){
				
				RootOpcionesVisual[i].SetActive(true);
				BotonesSeleccionSPT[i].transform.Find("Seleccion").GetComponent<Image>().enabled = true;

			}
			else{
				
				RootOpcionesVisual[i].SetActive(false);
				BotonesSeleccionSPT[i].transform.Find("Seleccion").GetComponent<Image>().enabled = false;
			
			}
		}
		 
		if (Zona0ErroFalse) {

			if (OpcionAElegir == 0){
				GameObject.FindObjectOfType<CalificarRetie2> ().EnZona0Error = true;
			}else {
				GameObject.FindObjectOfType<CalificarRetie2> ().EnZona0Error = false;
			}
		}

		PanelACerrar.Mostrar(false);

	}public void RandomSPT(){

		TelurometroConfigurado = false;
		PuedePinzasTelu = false;
		TelurometroActivado = false;
		BotonPicas.GetComponent<Button>().enabled = true;

		if( TelurometroBotón.transform.Find("Telurometro_Cerrar")){
			TelurometroBotón.transform.Find("Telurometro_Cerrar").gameObject.SetActive(false);
			TelurometroBotón.GetComponent<Button>().enabled = true;
		
		}
 
		PinzasRoot.SetActive(false);

		PinzaTelurometro = 0;
		zonaErrorSelected = -1;
		actualSelectedZ = -1;
		LastSelected = 0;
		picaActual = 0;

		Picas[0].SetActive(false);
		Picas[1].SetActive(false);
		
		int ranNum = Random.Range(0,SPT_Permitidos.Length-1); // spt random a activar de la lista disponible

		int pos = SPT_Permitidos[ranNum];

		// random resistencias de los SPT
		for(var i = 0; i< BotonesSeleccionSPT.Length; i++){

			BotonesSeleccionSPT[i].GetComponent<OhmioSPT>().CalcularResistencia();

		}

		RandomOhm();
 		OpcionAElegir = pos; // el spt que se elige al entrar en la zona
		if(OpcionAElegir == 0){

			SetZonaErrorSit0(0);

		}
		ValidarOpcion();
		RestaurarMedidasPorPica();

	}public void TomarPica(){

		if(picaActual == 0){

			// poner pica de distancia final
 

		}else{

			// pica variable
			InputDistancia.text = "";

		}

	}public void TomarMedida(){

		Vector3 lastPica1 = new Vector3(0f,0f,0f);
		// if (ZonaError && !Zona0ErroFalse && false)
		// 	DistanciaPica1 = (float)GameObject.FindObjectOfType<Graficadora>().GetHeightValue () * 6.5f; // porque la distancia de la zona 2 es variable por el random height que se genera

		float dist = 0f;
		if(InputDistancia.text.Length < 1){

			BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometro", "Ingresa un valor correcto, el campo no puede quedar vacío."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));

			// re activar cuando confirma y no ha ingresado ningún dato al campo de texto
			if(Picas[1].activeSelf & EnTelurometroParaPica){

				PinzasRoot.SetActive(true);
				TelurometroBotón.GetComponent<Button> ().enabled = false;
				TelurometroBotón.transform.GetChild(0).gameObject.SetActive(true);
				EnTelurometro(true);
				TelurometroBotón.GetComponent<Animator> ().SetBool ("activar", false);

				Resplandores[0].enabled = true;
				Resplandores[1].enabled = true;
				Resplandores[2].enabled = true;


			}
			return;

		}
		lastPica1 = Picas[1].transform.localPosition;
		if(picaActual == 0 && InputDistancia.text != "Ingresar distancia."){

			enunciado.text = textoPicasInput[0];
			dist = Mathf.Abs(float.Parse(InputDistancia.text));
								if (evaluarValorFijo)
								{
									// Si se calcula el valor fijo
									if (usarValorCalculado) valorFijo = (float)GameObject.FindObjectOfType<Graficadora>().GetHeightValue() * 6.5f; // Reemplazo el valor

									// Si la diferencia entre el valor fijo y la distancia dada, es menor al margen
									if (Mathf.Abs(dist - valorFijo) < margenDeErrorValorFijo)
									{
										Picas[picaActual].gameObject.SetActive(true);
										Picas[picaActual].transform.localPosition = new Vector3(dist, Picas[picaActual].transform.localPosition.y, Picas[picaActual].transform.localPosition.z);
										picaActual = 1;
										enunciado.text = textoPicasInput[1];
										RecogerDatos();
										return;
									}
								}

			//if(dist == DistanciaPica1){ // distancia correcta
			if(dist >= DistanciaPica1 && dist <=19){ // distancia correcta con rango

				Picas[picaActual].gameObject.SetActive(true);
				Picas[picaActual].transform.localPosition = new Vector3(dist,Picas[picaActual].transform.localPosition.y,Picas[picaActual].transform.localPosition.z);
				picaActual = 1;
				enunciado.text = textoPicasInput[1];
				RecogerDatos();
				return;

			}else{

				// mensaje de distancia incorrecta
				BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeDistanciaIncorrectaPinzasTelurometro","Distancia incorrecta de la pica de potencial."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
				return;
			}

		}else if(InputDistancia.text == "Ingresar distancia"){

			BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeDistanciaIncorrectaInputPinzasTelurometro","Distancia incorrecta."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
			return;

		}else{
			
			dist = Mathf.Abs(float.Parse(InputDistancia.text));

			if(dist>MaxValue){ // fuera de rango

				BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeDistanciaIncorrectaOFueraDeLimiteTelurometro","Distancia incorrecta o fuera de los limites."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
				if(Picas[1].activeSelf & EnTelurometroParaPica){ // re activar telurometro : se puede crear una función por si hay que replicar más este condicional

					PinzasRoot.SetActive(true);
					TelurometroBotón.GetComponent<Button> ().enabled = false;
					TelurometroBotón.transform.GetChild(0).gameObject.SetActive(true);
					EnTelurometro(true);
					TelurometroBotón.GetComponent<Animator> ().SetBool ("activar", false);

					Resplandores[0].enabled = true;
					Resplandores[1].enabled = true;
					Resplandores[2].enabled = true;

				}


				return; 
			}

			if(picaActual == 1){

				TelurometroBotón.GetComponent<Button> ().enabled = true;
				TelurometroBotón.GetComponent<Animator> ().SetBool ("activar", true);
				BotonPicas.GetComponent<Button>().enabled = false;

				enunciado.text = textoPicasInput[1];

				// re-activar telurometro
				if(Picas[1].activeSelf & EnTelurometroParaPica){
				
					PinzasRoot.SetActive(true);
					TelurometroBotón.GetComponent<Button> ().enabled = false;
					TelurometroBotón.transform.GetChild(0).gameObject.SetActive(true);
					EnTelurometro(true);
					TelurometroBotón.GetComponent<Animator> ().SetBool ("activar", false);

					Resplandores[0].enabled = true;
					Resplandores[1].enabled = true;
					Resplandores[2].enabled = true;


				}

			} 

			Picas[picaActual].gameObject.SetActive(true);
			Picas[picaActual].transform.localPosition = new Vector3(dist,Picas[picaActual].transform.localPosition.y,Picas[picaActual].transform.localPosition.z);
			picaActual = 1;


			// la misma función de recoger datos pero con mensajes

			float DistanciaTomada = Picas[0].transform.localPosition.x;
			float distanciaElectrica = Picas[1].transform.localPosition.x;
			bool correcto = false;

			for(var i = 0; i<9; i++){

				if(i == 5){//61.8%

					if(distanciaElectrica < (((DistanciaTomada/10) * (6.18f)) +(RangoError/100 * DistanciaTomada)) && 
						distanciaElectrica > (((DistanciaTomada/10) * (6.18f)) -(RangoError/100 * DistanciaTomada))){

						// guardar esta posición de medida correcta
						DatosRecolectados[i] = distanciaElectrica;
						//Debug.Log((((DistanciaTomada/10) * 1) +(RangoError/100 * DistanciaTomada)) + " - y - " +(((DistanciaTomada/10) * 1) -(RangoError/100 * DistanciaTomada)));
 						correcto = true;

					} 

				}else{

					if(distanciaElectrica < (((DistanciaTomada/10) * (i+1)) +(RangoError/100 * DistanciaTomada)) && 
						distanciaElectrica > (((DistanciaTomada/10) * (i+1)) -(RangoError/100 * DistanciaTomada))){

						// guardar esta posición de medida correcta
						DatosRecolectados[i] = distanciaElectrica;
						//Debug.Log((((DistanciaTomada/10) * 1) +(RangoError/100 * DistanciaTomada)) + " - y - " +(((DistanciaTomada/10) * 1) -(RangoError/100 * DistanciaTomada)));
 						correcto = true;

					} 

				}

			}if(correcto){



			}else{
				
				BotonPicas.GetComponent<Button>().enabled = true;	// reactivar el botón para activar la pica ya que al ingresar dato incorrecto la pica se va a ocultar
				//Picas[1].SetActive(false);  
				BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeDistanciaIncorrectaPicasPorcentajeTelurometro","Ingrese un valor de distancia correspondiente a los porcentajes de distancia total definidos en el procedimiento."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
				TelurometroConfigurado = false;
				TelurometroBotón.GetComponent<Button> ().enabled = false;
				Picas[picaActual].transform.localPosition = lastPica1;

				if(Picas[1].activeSelf & EnTelurometroParaPica){ // re activar telurometro

					PinzasRoot.SetActive(true);
					TelurometroBotón.GetComponent<Button> ().enabled = false;
					TelurometroBotón.transform.GetChild(0).gameObject.SetActive(true);
					EnTelurometro(true);
					TelurometroBotón.GetComponent<Animator> ().SetBool ("activar", false);

					Resplandores[0].enabled = true;
					Resplandores[1].enabled = true;
					Resplandores[2].enabled = true;

				}

				return;

			}




			RecogerDatos();

		}
 


	}public void ActualizarPica(int pos){

		if(pos == 0){



		}else{


			 
		}

	}public void RecogerDatos(){

		if(enTelurometro && TelurometroConfigurado && TelurometroActivado){

			float DistanciaTomada = Picas[0].transform.localPosition.x;
			float distanciaElectrica = Picas[1].transform.localPosition.x;


			for(var i = 0; i<9; i++){

				if(i == 5){//61.8%

					if(distanciaElectrica < (((DistanciaTomada/10) * (6.18f)) +(RangoError/100 * DistanciaTomada)) && 
						distanciaElectrica > (((DistanciaTomada/10) * (6.18f)) -(RangoError/100 * DistanciaTomada))){

						// guardar esta posición de medida correcta
						DatosRecolectados[i] = distanciaElectrica;
						//Debug.Log((((DistanciaTomada/10) * 1) +(RangoError/100 * DistanciaTomada)) + " - y - " +(((DistanciaTomada/10) * 1) -(RangoError/100 * DistanciaTomada)));
						calcularResitencia(distanciaElectrica,i,DistanciaTomada,BotonesSeleccionSPT[OpcionAElegir].GetComponent<OhmioSPT>().GetResistencia(), zonaErrorSelected);

						if(!NoAlterarPosMedidas)
							ReferenciasMedidas[i].transform.localPosition = new Vector3(Pinzas[2].transform.localPosition.x,ReferenciasMedidas[i].transform.localPosition.y,ReferenciasMedidas[i].transform.localPosition.z);// actualizar la posición de las medidas que se dibujan

					}

				}else{
					
					if(distanciaElectrica < (((DistanciaTomada/10) * (i+1)) +(RangoError/100 * DistanciaTomada)) && 
						distanciaElectrica > (((DistanciaTomada/10) * (i+1)) -(RangoError/100 * DistanciaTomada))){

						// guardar esta posición de medida correcta
						DatosRecolectados[i] = distanciaElectrica;
						//Debug.Log((((DistanciaTomada/10) * 1) +(RangoError/100 * DistanciaTomada)) + " - y - " +(((DistanciaTomada/10) * 1) -(RangoError/100 * DistanciaTomada)));
						calcularResitencia(distanciaElectrica,i,DistanciaTomada,BotonesSeleccionSPT[OpcionAElegir].GetComponent<OhmioSPT>().GetResistencia(), zonaErrorSelected);
	 					
						if(!NoAlterarPosMedidas)
							ReferenciasMedidas[i].transform.localPosition = new Vector3(Pinzas[2].transform.localPosition.x,ReferenciasMedidas[i].transform.localPosition.y,ReferenciasMedidas[i].transform.localPosition.z);// actualizar la posición de las medidas que se dibujan
					}

				}

			}

			bool verificar = VerifyData();

			// activar gráfica
			if(verificar){

				DatosCompletos();

			}else{

				BotonGrafico.GetComponent<Button>().interactable=false;

			}
				
		}

	}

	private bool VerifyData()
	{
		int length = DatosRecolectados.Length;
		for (int i = 0; i < length; i++)
		{
			if (DatosRecolectados[i] <= 0) return false;
		}
		return true;
	}

	public void PinzasTelurometro(int idPinzas)
	{

		if(idPinzas == -1){
			PuedePinzasTelu = false;
			Pinzas[PinzaTelurometro].transform.GetChild(0).GetComponent<Image>().sprite = PinzasEstado[PinzaTelurometro+3];
		}else
			PuedePinzasTelu = true;
		
		PinzaTelurometro = idPinzas;
		yaPuedePinza = true;
		
	}public void EnTelurometro(bool state){

		enTelurometro = state;

		// por si puso la varilla en una posición correcta no tener que volver a actualizar la posición
		if(enTelurometro)
			RecogerDatos();

	}public void DatosCompletos(){

		BotonGrafico.GetComponent<Button>().interactable=true;
		GameObject.FindObjectOfType<Graficadora>().SetDatosGraficar(DatosParaGraficar(),DatosRecolectados);

	}public void VerificarCofigPinzas(bool verify = false){

		// array de pinzas y array de picas
		bool PuedeActivar = true;
		//botonesTelu[0].GetComponent<Button>().enabled = TelurometroConfigurado;

		if((!(Vector3.Distance( Pinzas[0].transform.position, RootOpcionesVisual[0].transform.position)<DistVerEnlace) && !PinzaParaMalla) || (!PinzaEnMalla && PinzaParaMalla)){

			PuedeActivar = false;
			TelurometroConfigurado = PuedeActivar;
			if(verify)
				BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajePicaMalUbicadaTelurometro","No se ha realizado la conexión de las pinzas del telurómetro a las picas correspondientes."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
			return;
		}if(!(Vector3.Distance(Pinzas[1].transform.position,Picas[0].transform.position)<DistVerEnlace)){
			PuedeActivar = false;
			TelurometroConfigurado = PuedeActivar;
			if(verify)
				BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajePicaMalUbicadaTelurometro","No se ha realizado la conexión de las pinzas del telurómetro a las picas correspondientes."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
			return;
		}if(!(Vector3.Distance(Pinzas[2].transform.position,Picas[1].transform.position)<DistVerEnlace)){
			PuedeActivar = false;
			TelurometroConfigurado = PuedeActivar;
			if(verify)
				BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajePicaMalUbicadaTelurometro","No se ha realizado la conexión de las pinzas del telurómetro a las picas correspondientes."),DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar","ACEPTAR"));
			return;
		}

		TelurometroConfigurado = PuedeActivar;
		//botonesTelu[0].GetComponent<Button>().enabled = TelurometroConfigurado;

	}public void SetZonaError(int pos){
		
 		if(zonaErrorSelected == -1)
			zonaErrorSelected = pos;

		actualSelectedZ = pos;

		// como son las zonas internas del transformador, se carga la distancia para la pica con el número que se genera en el registro de datos y la graficadora
		DistanciaPica1 = (float)GameObject.FindObjectOfType<Graficadora>().GetHeightValue () * 6.5f;


		if(MiZona == zonaErrorSelected)
			ZonaError = true;

		if(telurosZona2[0] && telurosZona2[1] && telurosZona2[2]){

			telurosZona2[0].SetZonaErrorOtros(pos);
			telurosZona2[1].SetZonaErrorOtros(pos);
			telurosZona2[2].SetZonaErrorOtros(pos);

		}

	}public void SetZonaErrorOtros(int pos){

		if(zonaErrorSelected == -1)
			zonaErrorSelected = pos;

		actualSelectedZ = pos;


	}public int GetZonaSelected(){

		return actualSelectedZ;

	}public int	GetzonaError(){

		return zonaErrorSelected;

	}public void UpdatePinzaRootPos(int pos){

		PinzasReference.transform.localPosition = Posiciones[pos].localPosition;

	}public void ReiniciarPinzas(){

		// restaurar la posición inicial de las pinzas
		if (iniciado) {
		Pinzas [0].transform.localPosition = new Vector3( PinzasStartPos [0].x,PinzasStartPos [0].y,Pinzas [0].transform.localPosition.z);
		Pinzas [1].transform.localPosition = new Vector3( PinzasStartPos [1].x,PinzasStartPos [1].y,Pinzas [1].transform.localPosition.z);
		Pinzas [2].transform.localPosition = new Vector3( PinzasStartPos [2].x,PinzasStartPos [2].y,Pinzas [2].transform.localPosition.z);
		}
		Pinzas [0].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (1,1,1);
		Pinzas [1].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (1,1,1);
		Pinzas [2].transform.GetChild (0).GetComponent<Image> ().gameObject.transform.localScale = new Vector3 (1,1,1);
		zonaErrorSelected = -1;
		yaPuedePinza = false;

	}public void TelurometroParaPica(bool state){

		EnTelurometroParaPica = state;

	}public void RestaurarMedidasPorPica(){

		foreach(GameObject obj in ReferenciasMedidas){

			obj.SetActive(false);

		}

	}public void SetZonaErrorSit00(int pos){

		SetZonaErrorSit0(pos);

	}


}
