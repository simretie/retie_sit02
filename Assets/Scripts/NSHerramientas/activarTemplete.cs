﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activarTemplete : MonoBehaviour {

    public GameObject templeteGuitarra;
    public GameObject templeteDirecto;

    public GameObject redTrenzada;
    public GameObject redAbierta;

   /// public bool puntoIntercambiable;

    [SerializeField]
    private puntosEvaluacion PuntoEvaGuitarra;
    [SerializeField]
    private puntosEvaluacion PuntoEvaDierecto;

    [SerializeField]
    private PuntoFLexometro PuntoAintercambarGuitarra;
    [SerializeField]
    private PuntoFLexometro PuntoAintercambarTempleteSimple;


    // Use this for initialization
    void Start () {
      // activarRedTrensada(false);

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void activarTempleteGuitarra(bool activar)
    {
        if (activar)
        {
            PuntoEvaGuitarra.puntoDelFlexometro[0] = PuntoAintercambarGuitarra;
            PuntoEvaGuitarra.activo = true;
            PuntoEvaDierecto.activo = false;
 

        }
        else
        {
            PuntoEvaDierecto.puntoDelFlexometro[0] = PuntoAintercambarTempleteSimple;
            PuntoEvaGuitarra.activo = false;
            PuntoEvaDierecto.activo = true;
        }

        templeteGuitarra.SetActive(activar);
        templeteDirecto.SetActive(!activar);
    }

    /// <summary>
    /// desactiva todo
    /// </summary>
    public void reiniciar()
    {
        templeteGuitarra.SetActive(false);
        templeteDirecto.SetActive(false);
        redTrenzada.SetActive(false);
        redAbierta.SetActive(false);

    }

    /// <summary>
    /// activa una red ya sea trensada si sele da true o abierta si lo contrario
    /// </summary>
    /// <param name="EsTrenzada"></param>
    public void activarRedTrensada(bool EsTrenzada)
    {
        redTrenzada.SetActive(EsTrenzada);
        redAbierta.SetActive(!EsTrenzada);
    }

}
