using NsSeguridad;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using System.Globalization;

namespace NSEvaluacion
{
    public class Evaluacion : MonoBehaviour
    {
        #region members

        /// <summary>
        /// Rango de calificacion con posibilidad de definicion, ejemplo, hasta 5 cuando el array es definido con un solo elemento y este es numerico, ó de (5 - 10) 
        /// Cuando el array es definido con 2 elementos numericos, siendo el numero en el indice 0 la la nota mas baja y el numero en el indice 1 la nota mas alta 
        /// ó A - B - C - D - E si los elementos del array son definidos como letras, minimo 2 letras
        /// </summary>
        [SerializeField]
        private string[] rangoCalificacion;
        /// <summary>
        /// Rango de porcentaje para los intervalos
        /// </summary>
        [SerializeField]
        private float[] rangoPorcentaje5Letras;
        /// <summary>
        /// Clase que contiene las calificaciones de la situacion actualmente ejecutandose, esta clase se crea cada vez que se entra en una situación.
        /// </summary>
        [SerializeField]
        private CalificacionSituacion refCalificacionSituacion;
        /// <summary>
        /// Referencia a la clase seguridad para obtener la ruta del XML
        /// </summary>
        [SerializeField]
        private ClsSeguridad refClsSeguridad;
        /// <summary>
        /// Envio PDF
        /// </summary>
        [SerializeField]
        private clsEnvioPdf EnvioPdf;
        #endregion

        #region accesors

        public CalificacionSituacion _refCalificacionSituacion
        {
            get
            {
                return refCalificacionSituacion;
            }
        }
        #endregion

        #region public methods

        /// <summary>
        /// Traduce la calificacion total de la situacion actual a la calificacion que le corresponde en el rango definido
        /// </summary>
        public string GetCalificacionTotalRango()
        {
            CargarNotasDesdeXML();

            if (rangoCalificacion.Length == 0)
                return (-1f).ToString();

            float tmpMenorValorFloatRango;
            float tmpMayorValorFloatRango;

            var tmpRangoTipoNumerico = false;

            if (float.TryParse(rangoCalificacion[0], out tmpMenorValorFloatRango))//se puede parsear el elemento en el indice 0 del rango a tipo float?
                tmpRangoTipoNumerico = true;//el rango es tipo numerico

            if (tmpRangoTipoNumerico)
            {
                if (rangoCalificacion.Length > 2)
                {
                    Debug.LogError("La cantidad elementos del rango de calificacion del tipo numerico supera los 2 elementos, " +
                        "por lo que no se puede calcular un rango de calificacion adecuado, " +
                        "porfavor asigne para rangos numericos de calificacion un solo elemento numerico para calificacion maxima desde cero hasta el elemento numerico asignado, " +
                        "o asigne dos elementos numericos donde el primer elemento sera la nota minima y el segundo la nota maxima");

                    return (-1f).ToString();
                }
                else if (rangoCalificacion.Length == 2)
                {
                    if (!float.TryParse(rangoCalificacion[1], out tmpMayorValorFloatRango))
                    {
                        Debug.LogError("El indice 1 del rango de calificacion no es de tipo numerico por lo que no concuerda con el valor del indice 0 que si es tipo numerico");
                        return (-1).ToString();
                    }
                    EnvioPdf.ValorMayor = tmpMayorValorFloatRango;
                    return Mathf.Max(refCalificacionSituacion.GetCalificacionTotal() * tmpMayorValorFloatRango, tmpMenorValorFloatRango).ToString("0.0");//retorna el maximo entre la calificacion minima del rango definido y la calificacion lograda por el usuario 
                }
                else if (rangoCalificacion.Length == 1)
                {
                    return (refCalificacionSituacion.GetCalificacionTotal() * tmpMenorValorFloatRango).ToString();
                }
                else
                {
                    Debug.LogError("Rango calificacion no definido");
                    return "null";
                }
            }
            else
            {
                if (rangoCalificacion.Length == 5)
                {
                    var tmpIndiceLetra = 0;

                    if (rangoPorcentaje5Letras.Length < 5)
                    {
                        Debug.LogError("Rango de porcentajes para 5 letras no definido");
                        return "null";
                    }

                    for (int i = 0; i < rangoPorcentaje5Letras.Length; i++)                    
                        if (refCalificacionSituacion.GetCalificacionTotal() <= rangoPorcentaje5Letras[i])
                        {
                            tmpIndiceLetra = i;
                            break;                                            
                        }

                    return rangoCalificacion[tmpIndiceLetra];
                }
                else
                {
                    var tmpPorcentajeDistanciaActual = 1f;
                    var tmpPorcentajeConteo = 0f;
                    var tmpCantidadPorcentajeAumento = 1f / rangoCalificacion.Length;//cuanto de porcentaje aumenta por cada letra
                    var tmpIndiceLetra = 0;

                    for (int i = (rangoCalificacion.Length - 1); i >= 0; i--)
                    {
                        var tmpPorcentajeDistanciaCalculado = Mathf.Abs(refCalificacionSituacion.GetCalificacionTotal() - tmpPorcentajeConteo);

                        if (tmpPorcentajeDistanciaActual > tmpPorcentajeDistanciaCalculado)
                        {
                            tmpPorcentajeDistanciaActual = tmpPorcentajeDistanciaCalculado;
                            tmpIndiceLetra = i;
                        }

                        tmpPorcentajeConteo += tmpCantidadPorcentajeAumento;
                    }

                    return rangoCalificacion[tmpIndiceLetra];
                }                
            }
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion para la seleccion de la funcion de graficado
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarFuncionGraficadoCorrecta(float argCalificacion)
        {
            refCalificacionSituacion._funcionGraficadoCorrecta = argCalificacion;
            Debug.Log("AsignarFuncionGraficadoCorrecta : " + argCalificacion);
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion para el registro de los datos
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarRegistroDatosCorrectos(float argCalificacion)
        {
            refCalificacionSituacion._registroDatosCorrectos = argCalificacion;
            Debug.Log("AsignarRegistroDatosCorrectos : " + argCalificacion);
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion de las preguntas respondidas 
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarPreguntasEvaluacionCorrectas(float argCalificacion)
        {
            refCalificacionSituacion._preguntasEvaluacionCorrectas = argCalificacion;
            Debug.Log("AsignarPreguntasEvaluacionCorrectas : " + argCalificacion);
        }

        /// <summary>
        /// Asigna a la clase refCalificacionSituacion actual el valor de la calificacion con respecto a los intentos que a usado el usuario
        /// </summary>
        /// <param name="argCalificacion"></param>
        public void AsignarCantidadIntentos(float argCalificacion)
        {
            refCalificacionSituacion._cantidadIntentos = argCalificacion;
            Debug.Log("AsignarCantidadIntentos : " + argCalificacion);
        }

        /// <summary>
        /// Crea un objeto de calificacion nuevo, esto debe usarse cada vez que el usuario selecciona una situacion
        /// </summary>
        public void NuevaCalificacion()
        {
            refCalificacionSituacion = new CalificacionSituacion()
            {
                _cantidadIntentos = 0,
                _funcionGraficadoCorrecta = 0,
                _preguntasEvaluacionCorrectas = 0,
                _registroDatosCorrectos = 0
            };
        }

        #endregion

        #region private methods 

        private void CargarNotasDesdeXML()
        {

#if !UNITY_WEBGL
            var tmpXmlDocument = new XmlDocument();

            try
            {
            tmpXmlDocument.Load(refClsSeguridad._fileName);
            var tmpListRangos = new List<string>();
           
            XmlNodeList tmpXmlNodeList = tmpXmlDocument.DocumentElement.GetElementsByTagName("rango");

            for (int i = 0; i < tmpXmlNodeList.Count; i++)
                tmpListRangos.Add(tmpXmlNodeList[i].InnerXml);

            if (tmpListRangos.Count > 0)
                rangoCalificacion = tmpListRangos.ToArray();

            Debug.LogError(rangoCalificacion.Length+ "length" + tmpXmlNodeList.Count);
            }catch (System.Exception e)
            {
                Debug.Log("No se encontro el archivo XML, se usaran las notas por default|| Excepcion : "+ e);
            }
#endif
        }
#endregion
    }
}