﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstructuraEvaluarPunto:MonoBehaviour  {

    public PuntoFLexometro[] puntoDelFlexometro;
    public int IdAlquePertenece;

    /// <summary>
    /// retorna 1 si todos los puntos del vector tiene el mismo valor 
    /// </summary>
    /// <returns></returns>
    public float calcularValorCorrecto(bool respuesta)
    {
        float valorARetornar = 0f;
        int tam = puntoDelFlexometro.Length;
        var valorPorRespuestaCorrecta = 1 / tam;
        for (int i = 0; i < tam; i++)
        {
            if (puntoDelFlexometro[i].EsCorrectoELValorDado() == respuesta)
                valorARetornar = +valorPorRespuestaCorrecta;

        }
        return valorARetornar;

    }


}
