﻿using System.Collections;
using UnityEngine;
using NSInterfaz;
#if !UNITY_WEBGL
#endif
using NSBoxMessage;
using NsSeguridad;


namespace NSTraduccionIdiomas
{
    /// <summary>
    /// Clase que controla la seleccion de idiomas
    /// </summary>
    public class ClsControladorIdiomas : MonoBehaviour
    {
        public PanelInterfazSeleccionLenguajeInglesEspaniol refPanelInterfazSeleccionLenguajeInglesEspaniol;

        public ClsSeguridad seguridad;

        public bool bilingueIngEsp;

        public enum idioma
        {
            espaniol,
            ingles,
            portugues
        }

        public idioma IdiomaActual;

        private void Start()
        {
            // refPanelInterfazBienvenida.Mostrar(true);
            // DiccionarioIdiomas._instance.EventOnDiccionarioCargados.AddListener(ActivarVentanaCorrespondiente);
            ActivarVentanaCorrespondiente();

        }

        private void HandleDelegateOnBoxMessageButtonAccept()
        {
            Application.Quit();
        }

        private void ActivarVentanaCorrespondiente()
        {
            StartCoroutine(CouActivarPanel());
        }

        private IEnumerator CouActivarPanel()
        {
            yield return new WaitForSeconds(0.5f);

            switch (IdiomaActual)
            {
                case idioma.espaniol:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.espaniol);
                    break;

                case idioma.ingles:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.ingles);
                    break;

                case idioma.portugues:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.portugues);
                    break;
            }

            if (bilingueIngEsp)
                refPanelInterfazSeleccionLenguajeInglesEspaniol.Mostrar(true);
            else
                seguridad.mtdIniciarSimulador();
        }
    }
}
