﻿using NSSingleton;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace NSTraduccionIdiomas
{
    /// <summary>
    /// Carga el idioma desde archivos XML
    /// </summary>
    public class DiccionarioIdiomas : AbstractSingleton<DiccionarioIdiomas>
    {
        #region members

        /// <summary>
        /// Diccionario para las traducciones al idioma ingles
        /// </summary>
        public Dictionary<string, string> diccionarioIngles = new Dictionary<string, string>();

        /// <summary>
        /// Diccionario para las traducciones al idioma portugues
        /// </summary>
        public Dictionary<string, string> diccionarioPortugues = new Dictionary<string, string>();

        /// <summary>
        ///  Diccionario para las traducciones al idioma Español
        /// </summary>
        public Dictionary<string, string> diccionarioEspaniol = new Dictionary<string, string>();

        /// <summary>
        /// contiene el nombre de los arhivos empezando por español seguido de ingles y asi dependeiendo del la cantidad de lenguajes
        /// </summary>
        public string[] NombreArchivoACargar;

        /// <summary>
        /// Idioma seleccionado actualmente
        /// </summary>
        private ListaIdiomas idiomaActual;

        /// <summary>
        /// Para notificar cuando un diccionario fue cargado
        /// </summary>
        public UnityEvent EventOnDiccionarioCargados;

        /// <summary>
        /// si esto es igual a 2 se debe notificar que todos los diccionarios fueron cargados
        /// </summary>
        private int cantidadDiccionariosCargados = 0;
        #endregion

        #region delegates

        /// <summary>
        /// Delegado al que se suscribe el metodo que traduce los objetos con el componente TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">Clase para obtener el texto para traducir</param>
        private delegate void delegateTraduccion(TextParaTraducir argTextParaTraducir);

        private delegateTraduccion DltTraduccion = delegate (TextParaTraducir argTextParaTraducir) { };

        /// <summary>
        /// Delegado al que se suscribe el metodo que traduce un string
        /// </summary>
        /// <param name="argStringParaTraducir">String que se desea traducir</param>
        private delegate string delegateTraduccionString(string argNumbreTextParaTraducir, string texto);

        private delegateTraduccionString DltTraduccionString = delegate (string argNumbreTextParaTraducir, string texto) { return ""; };

        /// <summary>
        /// delegado al que se suscriben el metodo que traduce un text por su object name
        /// </summary>
        public delegate void delegateTraducirporNombreDelObjeto();

        public delegateTraducirporNombreDelObjeto DLTraducirForNameObj = delegate () { };
        #endregion

        #region monoBehaviour

        private void Awake()
        {
            SetIdioma(ListaIdiomas.espaniol);
            CargarDiccionarios();
        }
        #endregion

        #region private methods

        /// <summary>
        ///  metodo para traducir strings a español
        /// </summary>
        /// <param name="argStringParaTraducir">Nombre del objeto que tiene el text para traducir o llave</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionEspaniol(string argNombreTextParaTraducir, string texto)
        {
            if (diccionarioEspaniol.ContainsKey(argNombreTextParaTraducir))
                return diccionarioEspaniol[argNombreTextParaTraducir];
            else
                TrackingTextoSinTraduccion(argNombreTextParaTraducir);

            return texto;
        }

        /// <summary>
        /// Consigue la traduccion en ingles del texto de TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        private void GetTraduccionIngles(TextParaTraducir argTextParaTraducir)
        {
            if (diccionarioIngles.ContainsKey(argTextParaTraducir._textoOriginalEspaniol))
                argTextParaTraducir.text = diccionarioIngles[argTextParaTraducir._textoOriginalEspaniol];
            else
                TrackingTextoSinTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionIngles(string argNumbreTextParaTraducir, string texto)
        {
            if (diccionarioIngles.ContainsKey(argNumbreTextParaTraducir))
                return diccionarioIngles[argNumbreTextParaTraducir];
            else
                TrackingTextoSinTraduccion(argNumbreTextParaTraducir);

            return texto;
        }

        /// <summary>
        /// Consigue la traduccion en ingles del texto de TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        private void GetTraduccionPortugues(TextParaTraducir argTextParaTraducir)
        {
            if (diccionarioPortugues.ContainsKey(argTextParaTraducir._textoOriginalEspaniol))
                argTextParaTraducir.text = diccionarioPortugues[argTextParaTraducir._textoOriginalEspaniol];
            else
                TrackingTextoSinTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionPortugues(string argNumbreTextParaTraducir, string texto)
        {
            if (diccionarioPortugues.ContainsKey(argNumbreTextParaTraducir))
            {
                var tmpTraduccion = diccionarioPortugues[argNumbreTextParaTraducir];
                Debug.Log("Tmp Traduccion" + tmpTraduccion);
                return diccionarioPortugues[argNumbreTextParaTraducir];
            }
            else
                TrackingTextoSinTraduccion(argNumbreTextParaTraducir);

            return texto;
        }

        /// <summary>
        /// Debug para encontrar facilmente el TextParaTraducir que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(TextParaTraducir argTextParaTraducir)
        {
            Transform tmpLastParent = argTextParaTraducir.transform;
            var tmpRutaObjetoSinTraduccion = "";

            while (tmpLastParent != null)
            {
                tmpRutaObjetoSinTraduccion += tmpLastParent.name + "/";
                tmpLastParent = tmpLastParent.parent;
            }

            Debug.LogWarning("Objeto sin traduccion en : " + tmpRutaObjetoSinTraduccion + " Con texto : " + argTextParaTraducir._textoOriginalEspaniol);
        }

        /// <summary>
        /// Debug para encontrar facilmente el string que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(string argStringParaTraducir)
        {
            Debug.LogWarning("String sin traduccion : " + argStringParaTraducir);
        }

        /// <summary>
        /// metodo que llena los diccionarios con las traducciones del xml
        /// </summary>
        private void CargarDiccionarios()
        {
            AsignarTraduccionesEspaniol();
            AsignarTraduccionesIngles();
        }

        /// <summary>
        /// Courutina para leer las traducciones de un XML
        /// </summary>
        /// <param name="NombreText">Nombre del archivo de texto.</param>
        /// <param name="argDiccionario">Diccionario al que se le van a agregar las traducciones.</param>
        private IEnumerator CouLeerTraduccionesXML(string NombreText, Dictionary<string, string> argDiccionario)
        {
            var dir = System.IO.Path.Combine(Application.streamingAssetsPath, NombreText + ".xml");
            XmlDocument reader = new XmlDocument();

#if UNITY_ANDROID
            dir = "jar:file://" + Application.dataPath + "!/assets/"+ NombreText + ".xml";
            Debug.Log(dir);
            WWW wwwfile = new WWW(dir);
            yield return wwwfile;

            if (!string.IsNullOrEmpty(wwwfile.error))            
                Debug.Log("no se encontro el archivo");

                reader.LoadXml(wwwfile.text);


#elif UNITY_WEBGL
            UnityWebRequest www = UnityWebRequest.Get(dir);
            yield return www.SendWebRequest();
            reader.LoadXml( www.downloadHandler.text);
#else
            reader.LoadXml(System.IO.File.ReadAllText(dir));
            yield return null;
#endif

            XmlNodeList _list = reader.ChildNodes[0].ChildNodes;

            for (int i = 0; i < _list.Count; i++)
                argDiccionario.Add(_list[i].Name, _list[i].InnerXml);

            cantidadDiccionariosCargados++;

            if (cantidadDiccionariosCargados == 2)
                EventOnDiccionarioCargados.Invoke();
        }
        #endregion

        #region Traducciones Español

        /// <summary>
        ///  Agregar las traduccion para el idioma Ingles desde el español
        /// </summary>
        private void AsignarTraduccionesEspaniol()
        {
            diccionarioEspaniol.Clear();
            StartCoroutine(CouLeerTraduccionesXML(NombreArchivoACargar[0], diccionarioEspaniol));
        }
        #endregion

        #region Traducciones ingles

        /// <summary>
        /// Agregar las traduccion para el idioma Ingles desde el español
        /// </summary>
        private void AsignarTraduccionesIngles()
        {
            diccionarioIngles.Clear();
            StartCoroutine(CouLeerTraduccionesXML(NombreArchivoACargar[1], diccionarioIngles));
        }
        #endregion

        #region Traducciones portugues 

        /// <summary>
        /// Agregar las traduccion para el idioma Portugues desde el español
        /// </summary>
        private void AsignarTraduccionesPortugues()
        {
            diccionarioPortugues.Clear();
            StartCoroutine(CouLeerTraduccionesXML(NombreArchivoACargar[2], diccionarioPortugues));
        }
        #endregion

        #region public methods

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un TextParaTraducir
        /// </summary>
        /// <param name="argTextParaTraducir">TextParaTraducir</param>
        public void Traducir(TextParaTraducir argTextParaTraducir)
        {
            if (idiomaActual == ListaIdiomas.espaniol)
                return;

            DltTraduccion(argTextParaTraducir);
        }

        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un string
        /// </summary>
        /// <param name="argNombreTextParaTraducir"> </param>
        /// <param name="texto">Texto que se va a traducir</param>
        public string Traducir(string argNombreTextParaTraducir, string texto)
        {
            return DltTraduccionString(argNombreTextParaTraducir, texto);
        }

        /// <summary>
        /// Asigna un idioma
        /// </summary>
        /// <param name="argIdioma">Idioma que se usara</param>
        public void SetIdioma(ListaIdiomas argIdioma)
        {
            switch (argIdioma)
            {
                case ListaIdiomas.espaniol:
                    idiomaActual = argIdioma;
                    DltTraduccion = null;
                    DltTraduccionString = GetTraduccionEspaniol;
                    DLTraducirForNameObj();
                    break;

                case ListaIdiomas.ingles:
                    idiomaActual = argIdioma;
                    DltTraduccion = GetTraduccionIngles;
                    DltTraduccionString = GetTraduccionIngles;
                    DLTraducirForNameObj();
                    break;

                case ListaIdiomas.portugues:
                    idiomaActual = argIdioma;
                    DltTraduccion = GetTraduccionPortugues;
                    DltTraduccionString = GetTraduccionPortugues;
                    DLTraducirForNameObj();
                    break;
            }
        }
        #endregion
    }

    public enum ListaIdiomas
    {
        espaniol,
        ingles,
        portugues
    }
}