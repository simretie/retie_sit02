﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine.SceneManagement;
using NSInterfaz;
using NSInterfazAvanzada;

public class MangerZonas : MonoBehaviour {

    public clsCuaderno cuaderno;

    public ManagerZona5 mgZon5;
    public ControladorDeDatos mgsesion;
    public PanelInterfazEvaluacion panelPreguntas;
    public PanelRegistroDeDatos panelRegistroDatos;
    public ClsTomarFotoRegistoyFondo controladorDedatos;
    public panelZonas[] panelZonasConTempletesaleAtorios;

    [SerializeField]
    private panelZonas panelZonas7;

    [SerializeField]
    private ManagerZona7 mgZona7;

    [SerializeField]
    private GameObject[] postesSimples;

    [SerializeField]
    private GameObject[] postesEnH;

    [SerializeField]
    private GameObject[] postesZona6;

    [SerializeField]
    private GameObject[] postesZona7;

    [SerializeField]
    private ManagerFlexometro[] mgFlexometro;

    [SerializeField]
    private GameObject[] zonasQueTienenTempletesVariables;
    [SerializeField]
    private Calificar califica;

    public GameObject[] BotonesDeSeleccionZonas;
    public GameObject[] herramientas;
    public GameObject[] zonas;
    public GameObject ImagenesInicialZona5;

    ///----------
    public GameObject ImagenRedTrenzado;
    public GameObject ImagenaRedAbierto;

    /// <summary>
    /// define que red mostrar
    /// </summary>
    public bool EsTrenzado;
    //------------

    public bool mensajeDatosIncorrectos= true;
   
    public enum estadoSituacion5{

        NoRelizado,
        PosteEnHconDireccto,
        posteEnHconGuitarra,
        posteSImpleDirecto,
        posteSImpleGUitarra

    }

    public enum tipoPoste
    {
        EnH,
        simple
    }

    private int zonaActual=-1;

    private ManagerScene MgScenes;

  //  private GameObject canvasMeniIni;

    private GameObject camaraMenuInicio;

    private bool ActivoZona7;


        // Use this for initialization
	void Start () {

        cuaderno.initCuadreno();
        BotonesDeSeleccionZonas[7].SetActive(false);
        BotonesDeSeleccionZonas[6].SetActive(false);

        MgScenes = GameObject.FindGameObjectWithTag("ManagerScenes").GetComponent<ManagerScene>();
        camaraMenuInicio = GameObject.FindGameObjectWithTag("camaraMenuInicio");

        // canvasMeniIni = GameObject.FindGameObjectWithTag("canvasMenuIni");
        //canvasMeniIni.SetActive(false);
        // camaraMenuInicio.SetActive(false);
        MgScenes.offCanvasMenuInicio();
        MgScenes.recargarCanvasMesajes();
    }

/*
    private IEnumerator getCanvasMeniIni()
    {

        yield return new WaitForSeconds(1);
    }
*/
    // Update is called once per frame
    void Update () {

        if (mgZona7.zona7activa)
        {
            if (panelZonas7.ElTempleteEsguitarra)
            {
                postesZona7[0].SetActive(true);
                postesZona7[1].SetActive(false);
            }
            else
            {
                postesZona7[0].SetActive(false);
                postesZona7[1].SetActive(true);
            }
        }
        else
        {
            postesZona7[0].SetActive(false);
            postesZona7[1].SetActive(false);
        }
    }

    #region MethodPrivate

    /// <summary>
    /// Activa los botones de herramientas que se veran dentro de las zonas
    /// </summary>
    private void ActivarHerramientasPracticas(bool opcion) {

        herramientas[1].SetActive(opcion);
        herramientas[4].SetActive(opcion);
        herramientas[5].SetActive(!opcion);
        herramientas[6].SetActive(!opcion);
    }

    private void InicializartempletesDesona()
    {
        var tam= zonasQueTienenTempletesVariables.Length;

        for (int i=0;i<tam;i++)
            zonasQueTienenTempletesVariables[i].GetComponent<panelZonas>().decidirtemplete();

    }
    public void reiniciarFlexometros()
    {
        var tam = mgFlexometro.Length;
        
        for (int i = 0; i < tam; i++)
        {
            mgFlexometro[i].desactivarImgenDepuntosFLexometro();
            mgFlexometro[i].PuntosFlexometroActivos = false;
            // mgFlexometro[i].reiniciarPuntos();
        }
    }
    private void reiniciarFlexometrosDistancias()
    {
        var tam = mgFlexometro.Length;

        for (int i = 0; i < tam; i++)
        {
            mgFlexometro[i].desactivarImgenDepuntosFLexometro();
            mgFlexometro[i].reiniciarPuntos();
        }
    }
    #endregion

    #region MethodPublic

    /// <summary>
    /// metodo al cual sele pasa el estado en que quedo la situacion 5
    /// </summary>
    /// <param name="op"></param>
    public void actualizar(bool op)//estadoSituacion5 op )
    {
        BotonesDeSeleccionZonas[7].SetActive(op);
        BotonesDeSeleccionZonas[6].SetActive(op);
        /*switch (op) {

            case estadoSituacion5.NoRelizado:
                Debug.Log("aun no se cambiado");
                break;

            case estadoSituacion5.PosteEnHconDireccto:
                BotonesDeSeleccionZonas[7].SetActive(true);
                BotonesDeSeleccionZonas[6].SetActive(true);
                break;

            case estadoSituacion5.posteEnHconGuitarra:
                BotonesDeSeleccionZonas[7].SetActive(true);
                BotonesDeSeleccionZonas[6].SetActive(true);
                break;

            case estadoSituacion5.posteSImpleDirecto:
                BotonesDeSeleccionZonas[7].SetActive(true);
                BotonesDeSeleccionZonas[6].SetActive(true);
                break;

            case estadoSituacion5.posteSImpleGUitarra:
                BotonesDeSeleccionZonas[7].SetActive(true);
                BotonesDeSeleccionZonas[6].SetActive(true);
                break;
                
        }*/

    }

    public void EntrarZona(int NumeZona)
    {

        ActivarHerramientasPracticas(true);

        zonas[NumeZona].GetComponent<panelZonas>().Mostrar(true);

        zonaActual = NumeZona;

    }


    public void salirDeZona()
    {
        if (zonaActual != -1)
        {
            ActivarHerramientasPracticas(false);
            zonas[zonaActual].GetComponent<panelZonas>().Mostrar(false);
            reiniciarFlexometros();
            zonaActual = -1;
        }
        else {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeSalirSituacion", "¿Esta seguro que desea abandonar la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), salirDeLaPractica);
        }
    }

    public void salirDeLaPractica()
    {
        //SceneManager.LoadScene("Login");
        MgScenes.descargarpractica();
    }

    /// <summary>
    /// Esta funsion activa el poste elejido en la zona 5 en la vista superior de las zonas y retona el tipo de red
    /// </summary>
    public bool ActivarPosteZona5(tipoPoste Tipoposte,int IdDelElemento,bool TempleteGuitarra) {
        Debug.Log(Tipoposte + ", idleemnto: " + IdDelElemento);

        ImagenesInicialZona5.SetActive(false);
        actualizar(true);
        if (Tipoposte== tipoPoste.EnH) {

            if (TempleteGuitarra)
            {
                postesEnH[IdDelElemento].SetActive(true);
                EsTrenzado = Random.Range(0, 100) >65 ? true : false;
                pintarRed(true);
                return EsTrenzado;  
            }
            else
            {
                postesEnH[IdDelElemento + 3].SetActive(true);
                EsTrenzado = Random.Range(0, 100) > 65 ? true : false;
                pintarRed(true);
                return EsTrenzado;
            }

        }
        else
        {
            if (TempleteGuitarra)
            {
                postesSimples[IdDelElemento].SetActive(true);
                EsTrenzado = Random.Range(0, 100) > 65 ? false : true;
                pintarRed(true);
                return EsTrenzado;
            }
            else
            {
                postesSimples[IdDelElemento+3].SetActive(true);
                EsTrenzado = Random.Range(0, 100) > 65 ? false : true;
                pintarRed(true);
                return EsTrenzado;
            }
        }
      

    }

    public void pintarRed(bool activar)
    {
        if (activar)
        {
            if (EsTrenzado)
            {
                ImagenRedTrenzado.SetActive(true);
                ImagenaRedAbierto.SetActive(false);
            }
            else
            {
                ImagenRedTrenzado.SetActive(false);
                ImagenaRedAbierto.SetActive(true);
            }
        }
        else
        {
            Debug.Log("cuando sale abierto entra aqui");
            ImagenRedTrenzado.SetActive(false);
            ImagenaRedAbierto.SetActive(false);
        }

    }

    /// <summary>
    /// activa el poste determinado para esta zona
    /// </summary>
    /// <param name="templeteguitarra"></param>
    public void ActivarPosteZona7(bool templeteguitarra)
    {
        if (templeteguitarra)
            postesZona7[0].SetActive(true);
        else
            postesZona7[1].SetActive(true);
    }

    public void activarPosteZona6(bool templeteguitarra)
    {
        if (templeteguitarra)
            postesZona6[0].SetActive(true);
        else
            postesZona6[1].SetActive(true);
    }

    public void ReiniciarMapa()
    {
        int tam = postesSimples.Length;
        for(int i=0;i<tam;i++) {
            postesSimples[i].SetActive(false);
            postesEnH[i].SetActive(false);
        }
        if (zonaActual != -1)
        {
            ActivarHerramientasPracticas(false);
            zonas[zonaActual].GetComponent<panelZonas>().Mostrar(false);

            zonaActual = -1;
        }
        mgZona7.zona7activa = false;
        mgsesion.IniciarNuevaSesionSituacion();
        califica.reiniciarRegistroDeDatos();
        BotonesDeSeleccionZonas[7].SetActive(false);
        BotonesDeSeleccionZonas[6].SetActive(false);

        postesZona7[0].SetActive(false);
        postesZona7[1].SetActive(false);
        postesZona6[0].SetActive(false);
        postesZona6[1].SetActive(false);

        mgZona7.reiniciarLetrero();

        reiniciarTempletes();
        ImagenesInicialZona5.SetActive(true);
        // reiniciarFlexometros();
        reiniciarFlexometrosDistancias();
        InicializartempletesDesona();
        mgZon5.ReIniciar();
        actualizar(false);
    }

    public void PasarAgenerarReporte()
    {
        panelRegistroDatos.Mostrar(false);
        activarPreguntas();
       /* if(mensajeDatosIncorrectos)
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Los datos ingresados no son correctos, revise los datos y haga clic de nuevo en el botón verificar ¿Desea generar el reporte con estos datos?", "REPORTE", "CANCELAR", activarPreguntas);
        else
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("¿Esta seguro que desea generar el reporte?","ACEPTAR","CANCELAR", activarPreguntas);
*/

    }

    private void activarRdatos()
    {
        panelRegistroDatos.Mostrar(true);
    }

    private void activarPreguntas()
    {
        if (zonaActual != -1)
        {
            ActivarHerramientasPracticas(false);
            zonas[zonaActual].GetComponent<panelZonas>().Mostrar(false);

            zonaActual = -1;
        }
        controladorDedatos.capturarRegistroyFOndo();
        panelPreguntas.Mostrar(true);
    }

    private void reiniciarTempletes()
    {
        for (int i=0;i< panelZonasConTempletesaleAtorios.Length;i++)
        {
            panelZonasConTempletesaleAtorios[i].decidirtemplete();
        }
    }

    #endregion



}
