﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSBoxMessage;
using UnityEngine.UI;
using NSInterfaz;

public class ManagerZona5 : MonoBehaviour {

    public GameObject posteinicial;
    public MangerZonas managerZonas;
    
    public GameObject ObjetoPadrePosteH;
    public GameObject ObjetoPadrePosteSimple;

    public GameObject Flexometro;

    public Toggle[] BotonPostes;
    public Toggle[] BotonTemplete;

    [SerializeField]
    private PanelInterfazBienvenida explicacionZona5;

    [SerializeField]
    private GameObject[] postesSimples;
    [SerializeField]
    private GameObject contenerodPosteSimple;

    [SerializeField]
    private int esguitarraPosteSimple=-1;

    [SerializeField]
    private GameObject[] postesEnH;
    [SerializeField]
    private GameObject contenedorPostesEnH;
    [SerializeField]
    private int esguitarraPosteEnH=-1;


    [SerializeField]
    private int postesimpleElegido=-1;

    [SerializeField]
    private int posteEnHElegido=-1;



    [SerializeField]
    private puntosEvaluacion PuntoEva;

    [SerializeField]
    private PuntoFLexometro PuntoAintercambarPosteH;

    [SerializeField]
    private PuntoFLexometro PuntoAintercambarPosteSimple;

    private bool tipoDeRed;
    private bool validado;
    private bool YaEntreAlazona=false;
    // Use this for initialization
    void Start () {

        esguitarraPosteSimple = 1;
        postesimpleElegido =0;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnEnable()
    {
        /*if (!YaEntreAlazona)
        {
            explicacionZona5.Mostrar(true);
            
        }*/
    }

    public void activarExplicacionZona5()
    {
        if (!YaEntreAlazona)
        {
            explicacionZona5.Mostrar(true);

        }
    }

    /// <summary>
    /// metodo que me dice que poste fue elegido por el usuario 0=postesimple,1 poste en h, -1 aun no a seleccionado uno 
    /// </summary>
    /// <returns></returns>
    public int ElPosteElegidoEsEnH()
    {
        if (validado)
        {
            if (posteEnHElegido == -1 && postesimpleElegido != -1)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return -1;
        }

    }


    /// <summary>
    /// selecciona el poste simple con su id
    /// </summary>
    /// <param name="IdPOste"></param>
    public void ElegirPosteSimple(int IdPOste )
    {
        PuntoEva.puntoDelFlexometro[0] = PuntoAintercambarPosteSimple;
 
        posteEnHElegido =-1;
        postesimpleElegido = IdPOste;
    }
    /// <summary>
    /// selecciona el poste en h con su id
    /// </summary>
    /// <param name="IdPOste"></param>
    public void ElegirEnH(int IdPOste)
    {
        PuntoEva.puntoDelFlexometro[0] = PuntoAintercambarPosteH;
        posteEnHElegido = IdPOste;
        postesimpleElegido = -1;
    }

    /// <summary>
    /// se selecciona el poste simple 
    /// </summary>
    /// <param name="esguitarra"></param>
    public void ElegirSImpleTemplete(int esguitarra)
    {
        esguitarraPosteSimple = esguitarra;
        esguitarraPosteEnH = -1;
    }

    /// <summary>
    /// si es 0 es simple si es 1 es guitarra
    /// </summary>
    /// <param name="esguitarra"></param>
    public void ElegirEnHTemplete (int esguitarra)
    {
        esguitarraPosteSimple = -1;
        esguitarraPosteEnH = esguitarra;
    }

    public void validar()
    {
        
        if (esguitarraPosteSimple != -1 && postesimpleElegido != -1)
        {
            ///Lo que pasa cuando se elege poste simple con algun templete
            activarPosteSimple();
            posteinicial.SetActive(false);
            Flexometro.SetActive(true);
            validado = true;
            YaEntreAlazona = true;
        }
        else
        {
            if (esguitarraPosteEnH != -1 && posteEnHElegido != -1)
            {
                ///Lo que pasa cuando se eligen poste en H y un templete 
                activarposteEnH();
                posteinicial.SetActive(false);
                Flexometro.SetActive(true);
                validado = true;
                YaEntreAlazona = true;
            }
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo("Has elegido una combinación invalida ", "ACEPTAR");
            }

        }

    }


    /// <summary>
    /// activa la combinacion de poste simple elegida
    /// </summary>
    private void activarPosteSimple()
    {
        ObjetoPadrePosteSimple.SetActive(true);
        postesSimples[postesimpleElegido].SetActive(true);
        contenerodPosteSimple.SetActive(true);
        contenedorPostesEnH.SetActive(false);

        if (esguitarraPosteSimple == 0)
        {
           
            postesSimples[postesimpleElegido].GetComponent<activarTemplete>().activarTempleteGuitarra(false);
            tipoDeRed= managerZonas.ActivarPosteZona5(MangerZonas.tipoPoste.simple, postesimpleElegido, false);
           // managerZonas.pintarRed(tipoDeRed);
            postesSimples[postesimpleElegido].GetComponent<activarTemplete>().activarRedTrensada(tipoDeRed);
        }
        if (esguitarraPosteSimple == 1)
        {
            
            postesSimples[postesimpleElegido].GetComponent<activarTemplete>().activarTempleteGuitarra(true);
            tipoDeRed = managerZonas.ActivarPosteZona5(MangerZonas.tipoPoste.simple, postesimpleElegido, true);
            //managerZonas.pintarRed(tipoDeRed);
            postesSimples[postesimpleElegido].GetComponent<activarTemplete>().activarRedTrensada(tipoDeRed);
        }

    }


    private void activarposteEnH()
    {
        ObjetoPadrePosteH.SetActive(true);
        postesEnH[posteEnHElegido].SetActive(true);

        contenerodPosteSimple.SetActive(false);
        contenedorPostesEnH.SetActive(true);


        if (esguitarraPosteEnH == 0)
        {
            postesEnH[posteEnHElegido].GetComponent<activarTemplete>().activarTempleteGuitarra(false);
            tipoDeRed = managerZonas.ActivarPosteZona5(MangerZonas.tipoPoste.EnH, posteEnHElegido, false);
            postesEnH[posteEnHElegido].GetComponent<activarTemplete>().activarRedTrensada(tipoDeRed);

        }
        if (esguitarraPosteEnH == 1)
        {
            postesEnH[posteEnHElegido].GetComponent<activarTemplete>().activarTempleteGuitarra(true);
            tipoDeRed = managerZonas.ActivarPosteZona5(MangerZonas.tipoPoste.EnH, posteEnHElegido, true);
            postesEnH[posteEnHElegido].GetComponent<activarTemplete>().activarRedTrensada(tipoDeRed);

        }

    }

    public void ReIniciar()
    {
        YaEntreAlazona = false;
        esguitarraPosteSimple = 1;
        postesimpleElegido = 0;
        validado = false;
        posteEnHElegido = -1;
        esguitarraPosteEnH = -1;
        tipoDeRed = false;
        Flexometro.SetActive(false);
        posteinicial.SetActive(true);
        ObjetoPadrePosteH.SetActive(false);
        ObjetoPadrePosteH.SetActive(false);

        var tam = postesSimples.Length;
        for (int i=0;i< tam; i++) {
            postesSimples[i].GetComponent<activarTemplete>().reiniciar();
            postesEnH[i].GetComponent<activarTemplete>().reiniciar();
            postesSimples[i].SetActive(false);
            postesEnH[i].SetActive(false);
        }
        var tam2 = BotonPostes.Length;
        BotonPostes[0].isOn = true;
        for (int i=1;i<tam2;i++) {
            BotonPostes[i].isOn = false;

        BotonTemplete[0].isOn = true;
        var tam3 = BotonTemplete.Length;
        for (int b = 1; b < tam3; b++)
                BotonTemplete[b].isOn = false;
        }

    }



}
