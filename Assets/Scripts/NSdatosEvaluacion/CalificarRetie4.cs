﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalificarRetie4 : MonoBehaviour {

	// Use this for initialization

	public GameObject[] Opciones;
	public GameObject[] inputsDistancias;
	public SelectorRetie3[] Selectores;
	public float[] DistanciasCorrectas;
	public float[] DistanciasCorrectas1;
	public int[] RangosDistancias;
	public GameObject[] Flexometros;
	public PuntoFLexometro[] PuntosFlexo;
	public int FlexoInvertido = 1; // el flexometro que se le suma 0.5
	public bool CamposCorrectos = false;
	public bool EmptyCampos = false;

	[Header("Posibilidad 1")]
	public int[] RespuestaCorrecta1;
	public bool[] indexInverso1;
	public int[] EstadoInicial1;
	public bool[] HabilitarEdicion1;

	[Header("Posibilidad 2")]
	public int[] RespuestaCorrecta2;
	public bool[] indexInverso2;
	public int[] EstadoInicial2;
	public bool[] HabilitarEdicion2;

	[Header("Posibilidad 3")]
	public int[] RespuestaCorrecta3;
	public bool[] indexInverso3;
	public int[] EstadoInicial3;
	public bool[] HabilitarEdicion3;

	[Header("Posibilidad 4")]
	public int[] RespuestaCorrecta4;
	public bool[] indexInverso4;
	public int[] EstadoInicial4;
	public bool[] HabilitarEdicion4;

	public float Calificacion = 0f;
	public float NotaTotal = 0f;

	private int[][] Resultado = new int[4][];
	private bool[][] HabilitarEdicion = new bool[4][];
	public GameObject[] ZonasActivasBotones;

	[Header("Trafos")]

	public GameObject[] Trafos1; // placas internas en cada trafo con los datos diferentes
	public GameObject[] Trafos2;
	public GameObject[] Trafos3;
	public GameObject[] Trafos4;
	private GameObject[][] TrafosList = new GameObject[4][];

	[Header("Opciones de reinicio")]
	public GameObject[] ZonasPrincipales;
	public GameObject[] ZonasActivas;
	private int[][] SettingsList = new int[4][];
	private int randomSettings = 0;
	public GameObject[] Trafos;
	private int flexoHabilitado = 0;
	private int PotenciaTransformador = 0; // 1 para 34 kv

	void Start () {


		randomSettings = Random.Range (0,3);
		SettingsList [0] = EstadoInicial1;
		SettingsList [1] = EstadoInicial2;
		SettingsList [2] = EstadoInicial3;
		SettingsList [3] = EstadoInicial4;

		HabilitarEdicion [0] = HabilitarEdicion1;
		HabilitarEdicion [1] = HabilitarEdicion2;
		HabilitarEdicion [2] = HabilitarEdicion3;
		HabilitarEdicion [3] = HabilitarEdicion4;

		for(var i =0; i<Selectores.Length; i++){

			Selectores[i].RandomStart(SettingsList[randomSettings][i]);
			Selectores [i].EnableMod (HabilitarEdicion[randomSettings][i]);

		}

		//Habilitar Cajas
		for(var i = 0; i< ZonasActivasBotones.Length; i++){

			ZonasActivasBotones[i].SetActive(HabilitarEdicion[randomSettings][i]);


		}

		TrafosList [0] = Trafos1;
		TrafosList [1] = Trafos2;
		TrafosList [2] = Trafos3;
		TrafosList [3] = Trafos4;

		for(var i = 0; i<TrafosList.Length; i++){

 			for(var l = 0; l<Trafos1.Length; l++){

				if (i == randomSettings) {
					TrafosList [randomSettings] [l].SetActive (true); 
				} else {

					TrafosList [i] [l].SetActive (false);
				
				}
			}
		}
	}
	
	// Update is called once per frame
	public void Varificar () {

		CompararConfiguracion ();

	}public void CompararConfiguracion(){

		CamposCorrectos = false;
		EmptyCampos = false;

		Calificacion = 0;
		bool correcto = false;
		Resultado[0] = new int[Selectores.Length];
		// opción1
		for(var i = 0; i<Selectores.Length;i++){
			
			if(Selectores[i].ActualSeleccionado == RespuestaCorrecta1[i] && !indexInverso1[i]){ // 
				correcto = true;
				Resultado[0][i] = 1;
			}else if(Selectores[i].ActualSeleccionado != RespuestaCorrecta1[i] && indexInverso1[i]){ // si hay más de dos opciones, la correcta de la lista es la incorrecta
				correcto = true;
				Resultado[0][i] = 1;
			}else{
				correcto = false;
				Resultado[0][i] = 0;
			}
		}

		// opción 2
		Resultado[1] = new int[Selectores.Length];
		for(var i = 0; i<Selectores.Length;i++){

			if(Selectores[i].ActualSeleccionado == RespuestaCorrecta2[i] && !indexInverso2[i]){ // 
				correcto = true;
				Resultado[1][i] = 1;
			}else if(Selectores[i].ActualSeleccionado != RespuestaCorrecta2[i] && indexInverso2[i]){ // si hay más de dos opciones, la correceta de la lista es la incorrecta
				correcto = true;
				Resultado[1][i] = 1;
			}else{
				correcto = false;
				Resultado[1][i] = 0;
			}
		}

		// opción 3
		Resultado[2] = new int[Selectores.Length];
		for(var i = 0; i<Selectores.Length;i++){

			if(Selectores[i].ActualSeleccionado == RespuestaCorrecta3[i] && !indexInverso3[i]){ // 
				correcto = true;
				Resultado[2][i] = 1;
			}else if(Selectores[i].ActualSeleccionado != RespuestaCorrecta3[i] && indexInverso3[i]){ // si hay más de dos opciones, la correceta de la lista es la incorrecta
				correcto = true;
				Resultado[2][i] = 1;
			}else{
				correcto = false;
				Resultado[2][i] = 0;
			}
		}

		// opción 4
		Resultado[3] = new int[Selectores.Length];
		for(var i = 0; i<Selectores.Length;i++){

			if(Selectores[i].ActualSeleccionado == RespuestaCorrecta4[i] && !indexInverso4[i]){ // 
				correcto = true;
				Resultado[3][i] = 1;
			}else if(Selectores[i].ActualSeleccionado != RespuestaCorrecta4[i] && indexInverso4[i]){ // si hay más de dos opciones, la correceta de la lista es la incorrecta
				correcto = true;
				Resultado[3][i] = 1;
			}else{
				correcto = false;
				Resultado[3][i] = 0;
			}
		}
			
		ContarResupuesta();

	}public void ContarResupuesta(){

		float notaPorVista = 0.0375f;
		float notaTotalVistas = 0f;
		int correcto = -1;
		int[] tempList = new int[4];
		for(var i = 0; i<tempList.Length; i++){

			for(var l = 0; l<Selectores.Length; l++){

				if(Resultado[i][l] == 1){

					tempList[i]+=1;

				}

			}

			if(tempList[i] >= tempList.Length){//tiene los campos de esa posibilidad, correctos

				correcto = i;
				notaTotalVistas = notaPorVista*tempList[i];
			}

		}

		int actual = 0;
		if(correcto == -1){
			
			for(var i =0; i< tempList.Length-1; i++){ // calcular la zona que más coindidencias tiene

				if(tempList[i] > tempList[i+1])
					actual = i;
				else
					actual = i+1;
			}



		}else{

			// tiene todos los campos correcto

		}

		Calificacion += notaTotalVistas;
		CompararMedidas();
			
	}public void SetFlexoHabilitado(int flexo){

		// saber el estado inicial de los flexometros para activar los inputs correctos
		flexoHabilitado = flexo;

	}public void SetPotenciaTransformador(int Pot){

		// saber el estado inicial de los flexometros para activar los inputs correctos
		PotenciaTransformador = Pot;

	}public void CompararMedidas(){

		EmptyCampos = false;
		CamposCorrectos = true;
		int CantidadDistanciaMin = 0;// distancias correctas
		float notaPorCampo = 0.01563f;
		float NotaTotalCampos = 0f;

		// verificación de las distancias correcta en los inputs
		int minCorrectos = 0;
		for(var i =0; i<inputsDistancias.Length; i++){

			if(randomSettings == 0){
				if(inputsDistancias[i].GetComponentInChildren<InputField>().text == DistanciasCorrectas[i].ToString()  && PotenciaTransformador!=1){
					CantidadDistanciaMin+= 1;
					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					NotaTotalCampos += notaPorCampo;
					minCorrectos+=1;
					//CamposCorrectos = true; 
				}else if(inputsDistancias[i].GetComponentInChildren<InputField>().text == DistanciasCorrectas1[i].ToString()  && PotenciaTransformador==1){
					CantidadDistanciaMin+= 1;
					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					NotaTotalCampos += notaPorCampo;
					minCorrectos+=1;
					//CamposCorrectos = true; 
				}else{

					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = true;
					CamposCorrectos = false;

					 

				} 
			}else{

				if(inputsDistancias[i].GetComponentInChildren<InputField>().text == DistanciasCorrectas1[i].ToString()){
					CantidadDistanciaMin+= 1;
					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					NotaTotalCampos += notaPorCampo;
					minCorrectos +=1;
					//CamposCorrectos = true;
				}else{

					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = true;
					CamposCorrectos = false;

					 

				} 

			}

			if(minCorrectos >= 4){

				CamposCorrectos = true;

			}

		}

		float[] DistanciasDeLosPuntos = new float[8];


		/*
		for(var i=0; i<Flexometros.Length; i++){

			if(Flexometros[i].gameObject.activeInHierarchy){

				flexoHabilitado = i;

			}

		}*/

		bool[] DistanciasBien = new bool[4];
		if(flexoHabilitado == FlexoInvertido){

			DistanciasDeLosPuntos[4] = PuntosFlexo[flexoHabilitado*4].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[5] = PuntosFlexo[(flexoHabilitado*4)+1].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[6] = PuntosFlexo[(flexoHabilitado*4)+2].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[7] = PuntosFlexo[(flexoHabilitado*4)+3].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[0] = (PuntosFlexo[0].getVolorObtenidoAleatoriaMente()-0.5f);
			DistanciasDeLosPuntos[1] = (PuntosFlexo[1].getVolorObtenidoAleatoriaMente()-0.5f);
			DistanciasDeLosPuntos[2] = (PuntosFlexo[2].getVolorObtenidoAleatoriaMente()-0.5f);
			DistanciasDeLosPuntos[3] = (PuntosFlexo[3].getVolorObtenidoAleatoriaMente()-0.5f);

		}else{

			DistanciasDeLosPuntos[0] = PuntosFlexo[flexoHabilitado*4].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[1] = PuntosFlexo[(flexoHabilitado*4)+1].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[2] = PuntosFlexo[(flexoHabilitado*4)+2].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[3] = PuntosFlexo[(flexoHabilitado*4)+3].getVolorObtenidoAleatoriaMente();
			DistanciasDeLosPuntos[4] = PuntosFlexo[0].getVolorObtenidoAleatoriaMente()+0.5f;
			DistanciasDeLosPuntos[5] = PuntosFlexo[1].getVolorObtenidoAleatoriaMente()+0.5f;
			DistanciasDeLosPuntos[6] = PuntosFlexo[2].getVolorObtenidoAleatoriaMente()+0.5f;
			DistanciasDeLosPuntos[7] = PuntosFlexo[3].getVolorObtenidoAleatoriaMente()+0.5f;
			
		}for(var i = 0; i<4; i++){

			if(randomSettings == 0){
				if(DistanciasDeLosPuntos[i]>= DistanciasCorrectas[i] && flexoHabilitado != FlexoInvertido && PotenciaTransformador!=1){

					DistanciasBien[i] = true;
					NotaTotalCampos += notaPorCampo;


				}else if(DistanciasDeLosPuntos[i+4]>= DistanciasCorrectas[i+4] && PotenciaTransformador!=1){

					DistanciasBien[i] = true;
					NotaTotalCampos += notaPorCampo;

				}else if(DistanciasDeLosPuntos[i+4]>= DistanciasCorrectas1[i+4] && PotenciaTransformador==1){

					DistanciasBien[i] = true;
					NotaTotalCampos += notaPorCampo;

				}else{

					DistanciasBien[i] = false;

				}
			}else{

				if(DistanciasDeLosPuntos[i]>= DistanciasCorrectas1[i] && flexoHabilitado != FlexoInvertido){

					DistanciasBien[i] = true;
					NotaTotalCampos += notaPorCampo;


				}else if(DistanciasDeLosPuntos[i+4]>= DistanciasCorrectas1[i+4]){

					DistanciasBien[i] = true;
					NotaTotalCampos += notaPorCampo;

				}else{

					DistanciasBien[i] = false;

				}

			}

			if(flexoHabilitado != FlexoInvertido){

				if(Opciones[i].transform.GetComponentInChildren<Dropdown>().value == 0)
					EmptyCampos = true;

				if(Opciones[i].transform.GetComponentInChildren<Dropdown>().value == 1 && DistanciasBien[i]){// correcto
					Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
 				}else if(Opciones[i].transform.GetComponentInChildren<Dropdown>().value == 2 && !DistanciasBien[i]){// correcto
					Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
				}else{
					Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = true;
					CamposCorrectos = false;
				}
				// las opciones del NA
				if(Opciones[i+4].transform.GetComponentInChildren<Dropdown>().value == 3 ){// correcto
					Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					inputsDistancias[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					NotaTotalCampos += notaPorCampo;
 				}else{
					Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = true; // incorrecto
					inputsDistancias[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = true;
					//CamposCorrectos = false;
				}
			}

			if(flexoHabilitado == FlexoInvertido){

				if(Opciones[i+4].transform.GetComponentInChildren<Dropdown>().value == 0)
					EmptyCampos = true;

				// las opciones del NA
				if(Opciones[i].transform.GetComponentInChildren<Dropdown>().value == 3 ){// correcto
					Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
					NotaTotalCampos += notaPorCampo;
 				}else{
					Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = true; // incorrecto
					inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>().enabled = true;
					CamposCorrectos = false;
				}
				
				if(Opciones[i+4].transform.GetComponentInChildren<Dropdown>().value == 1 && DistanciasBien[i]){// correcto
	 				Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = false;
 				}else if(Opciones[i+4].transform.GetComponentInChildren<Dropdown>().value == 2 && !DistanciasBien[i]){// correcto
	 				Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = false;
				}else{
	 				Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = true;
					CamposCorrectos = false;
				}
			}

		}
 


		Calificacion += NotaTotalCampos;
		NotaTotal = Calificacion;

	}public float VerificarDatosUsuario(){

		Varificar ();
		return NotaTotal;

	}public void Reiniciar(){

		randomSettings = Random.Range (0,3);
		for(var i = 0; i<ZonasPrincipales.Length; i++){

			ZonasPrincipales[i].gameObject.SetActive(true);

		}for(var i =0; i<Selectores.Length; i++){

			Selectores[i].RandomStart(SettingsList[randomSettings][i]);
			Selectores [i].EnableMod (HabilitarEdicion[randomSettings][i]);
			Selectores[i].ReStart();

		}for(var i = 0; i<PuntosFlexo.Length; i++){

			PuntosFlexo[i].generarDistanciaDelPunto();

		}for(var i = 0; i<ZonasActivas.Length; i++){

			ZonasActivas[i].SetActive(true);

		}

		for(var i = 0; i<4; i++){
			
			Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
			Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = false;

		}
		for(var i = 0; i<Trafos.Length; i++){

			Trafos[i].SetActive(false);

		}
		//Habilitar Cajas
		for(var i = 0; i< ZonasActivasBotones.Length; i++){

			ZonasActivasBotones[i].SetActive(HabilitarEdicion[randomSettings][i]);


		}

		TrafosList [0] = Trafos1;
		TrafosList [1] = Trafos2;
		TrafosList [2] = Trafos3;
		TrafosList [3] = Trafos4;

		for(var i = 0; i<TrafosList.Length; i++){

			for(var l = 0; l<Trafos1.Length; l++){

				if (i == randomSettings) {
					TrafosList [randomSettings] [l].SetActive (true); 
				} else {

					TrafosList [i] [l].SetActive (false);

				}
			}
		}
	}
}
